﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UyuyanBerberProblemi
{
    class Program
    {
        //Semafor'da çalışmaya başlayacak thread sayısı,Semafor'un aynı anda max kaç thread çalıştıracağının sayısı
        static Semaphore koltuklar = new Semaphore(3,4);  //Berber koltuklarımız
        static Semaphore sandalyeler = new Semaphore(7, 8);//Sandalyelerimiz
        static Thread[] musteri = new Thread[15]; //Müşterilerimiz
       
        static void Main()
        {
            Random r = new Random();
            int musteriGelmeSuresi;           
            
            for (int i = 0; i < musteri.Length; i++)
            {
                musteriGelmeSuresi = r.Next(777777777, 999999999);

                for (int j = 0; j < musteriGelmeSuresi; j++);
                musteri[i] = new Thread(berberDukkani);
                musteri[i].Name = i + 1 + ".Musteri";
                musteri[i].Start();
            }
        }


        static void berberDukkani()
        {

            Console.WriteLine("{0} berber salonuna girdi.",Thread.CurrentThread.Name);
            Thread.Sleep(4000);
           
            if(koltuklar.WaitOne(0))//Koltuklar
            {
                Console.WriteLine("{0} berberi uyandırdı.", Thread.CurrentThread.Name);
                Thread.Sleep(4000);
                Console.WriteLine("{0} berber koltuğuna oturdu.", Thread.CurrentThread.Name);
                Thread.Sleep(4000);
                Console.WriteLine("{0} tıraş oluyor...", Thread.CurrentThread.Name);
                Thread.Sleep(8000);
                Console.WriteLine("{0}'nin tıraşı bitti.", Thread.CurrentThread.Name);
                Thread.Sleep(4000);
                Console.WriteLine("{0} koltuktan kalktı ve ücretini ödedi.", Thread.CurrentThread.Name);
                koltuklar.Release();
                Thread.Sleep(4000);
                Console.WriteLine("{0} salondan çıkış yaptı.", Thread.CurrentThread.Name);
                Thread.Sleep(4000);
                if ((int)sandalyeler.Release() == 8)
                {
                    sandalyeler.WaitOne();
                    Console.WriteLine("{0} 'nin berberi uyumaya gitti.ZzZzZ", Thread.CurrentThread.Name);
                    Thread.Sleep(4000);
                }
                else
                {
                    sandalyeler.WaitOne();
                }

            }
            else//Sandalye kısmı
            {
                if(sandalyeler.WaitOne(0))
                {
                    Console.WriteLine("{0} sandalyeye oturdu ve beklemeye başladı.", Thread.CurrentThread.Name);
                    koltuklar.WaitOne();
                    sandalyeler.Release();
                    Console.WriteLine("{0}'ye sıra geldi ve sandalyeden kalktı.", Thread.CurrentThread.Name);
                    Thread.Sleep(4000);
                    Console.WriteLine("{0} berber koltuğuna oturdu.", Thread.CurrentThread.Name);
                    Thread.Sleep(4000);
                    Console.WriteLine("{0} tıraş oluyor...", Thread.CurrentThread.Name);
                    Thread.Sleep(8000);
                    Console.WriteLine("{0}'nin tıraşı bitti.", Thread.CurrentThread.Name);
                    Thread.Sleep(4000);
                    Console.WriteLine("{0} koltuktan kalktı ve ücretini ödedi.", Thread.CurrentThread.Name);
                    Thread.Sleep(4000);
                    koltuklar.Release();
                    Console.WriteLine("{0} salondan çıkış yaptı.", Thread.CurrentThread.Name);
                    Thread.Sleep(4000);
                    if ((int)sandalyeler.Release() >= 7)
                    {
                        sandalyeler.WaitOne();

                        if (koltuklar.WaitOne(0))
                        {
                            Console.WriteLine("{0} 'nin berberi uyumaya gitti.ZzZzZ ", Thread.CurrentThread.Name);
                            Thread.Sleep(4000);
                            koltuklar.Release();
                            Console.ReadLine();
                        }
                    }
                    else
                    {
                        sandalyeler.WaitOne();
                                  
                    }
                }
                else
                {
                    Console.WriteLine("{0} boş sandalye bulamadı ve salonu terk etti !!!", Thread.CurrentThread.Name);
                }                         
            }
        }
    }  
}
